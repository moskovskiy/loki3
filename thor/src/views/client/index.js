import { useState } from 'react';
import axios from 'axios';
import LoginView from './Authorize';
import CardView from './Card'

function Client(props) {
    let [loggedIn, setLoggedIn] = useState(false)
    let [token, setToken] = useState(null)
    let [userObject, setUserObject] = useState(null)

    const handleChange = (newstate, newobject) => {
        console.log ("UPDATED APP STATE: ", newobject)
        setLoggedIn (newstate)
        setUserObject (newobject)
    }

    console.log("Axiosing ", "https://api4.simployal.com/c/" + props.match.params.point)

    if (token == null) {
        axios.get("https://api4.simployal.com/c/" + props.match.params.point).then((response) => {
            setToken(response.data.id)
            console.log("Parsed id: ", response.data.id)
        })
    }

    return (
        <div className="Client">
            {(!loggedIn) && <>
                <LoginView handleChange={handleChange} id={token}/>
            </>}
            {(loggedIn) && <>
                <CardView handleChange={handleChange} id={token} userObject={userObject}/>
            </>}
        </div>
    );
}

export default Client;