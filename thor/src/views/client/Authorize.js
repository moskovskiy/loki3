import { useState } from 'react';
import axios from 'axios';
import { GoogleLogin } from 'react-google-login';
import './Authorize.css';
import googleLoginIcon from '../../media/google.png';
import bg from '../../media/bg.png';

const clientId =
  '580914655403-dupoai4crpikhc1ip0kvidduqu2oir7e.apps.googleusercontent.com';

function Authorize(props) {

    let [pointObject, setObject] = useState(null)

    if ((pointObject == null) && (props.id != null)) {
            axios.get("https://api4.simployal.com/c/" + props.id + "/info").then((response) => {
                setObject(response.data)
                console.log("Parsed info: ", response.data)
        })
    }

    console.log("Resp: ", JSON.stringify(pointObject))

    return (
        <div className="Client--authorize">{props.id &&<>
        <img className="Client--background" src={bg}/>
        <img className="Client--logo" src={"https://api4.simployal.com/c/" + Number(props.id) + "/logo?size=big"}/>
        <div className="Client--login_header">{(pointObject)? pointObject.login_header : "Добро пожаловать!"}</div>
        <div className="Client--login_description">{(pointObject)? pointObject.login_description  : "Получайте бонусы"}</div>
        
        {(!pointObject) && <></>}
        
        <GoogleLogin
            clientId={clientId}
            buttonText="Continue with Google"
            onSuccess={(res) => {
                props.handleChange(true, res.profileObj)
                console.log("Logged in: " + JSON.stringify(res.profileObj))
            }}
            onFailure={(err) => {
                console.log(err)
            }}
            cookiePolicy={'single_host_origin'}
            style={{ marginTop: '100px' }}
            isSignedIn={true}

            render={renderProps => (
                <>{pointObject && <button className="Client--login_btn" onClick={renderProps.onClick}>
                    <img className="Client--login_btn--icon" src={googleLoginIcon}/>
                    <span className="Client--login_btn--text">
                        {(pointObject.language == "ru") && <>Продолжить с Google</>}
                        {(pointObject.language == "en") && <>Sign-in with Google</>}
                        {(pointObject.language == "pl") && <>Kontynuuj w Google</>}
                    </span>
                </button>}</>
            )}
        />
        </>}</div>

    )
}

export default Authorize;