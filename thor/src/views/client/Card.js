import { useState } from 'react';
import axios from 'axios';
import './Card.css';
import { GoogleLogout } from 'react-google-login'
import CoffeeIcon from './CoffeeIcon.js'

import igIcon from '../../media/instagram.png'
import rvIcon from '../../media/reviews.png'
import tsIcon from '../../media/tips.png'
import wsIcon from '../../media/website.png'

const clientId =
  '580914655403-dupoai4crpikhc1ip0kvidduqu2oir7e.apps.googleusercontent.com';

function calcColor (i, count, type) {
    if (i < count) return "#ffffff";
    if (i == (type-1)) return "#ee6c30";
    return "#888888";
}

function GenerateCups(props) {
    let seq = Array.from({length: props.type}, (_, i) => i + 1)
    return (
        <div>
            {
            seq.map ((e, i) => {
                return (
                    <>
                        {(i == Math.floor((props.type+1)/2)) && <br/>}
                        <CoffeeIcon key={i} className="Card--plastic--icon"  style={{marginRight:((200/props.type)+"px")}} color={calcColor(i, props.count, props.type)} width={Math.floor(100/props.type) + "vw"} height={"10vw"}/>
                    </>
                )
            })
        }</div>
    )
}


function GenerateCard (props) {
    if (!props.point || !props.coded) return(<></>)
    if (props.point.type == 0) return (<div className="Card--plastic--amount">{JSON.stringify(props.coded.amount)}</div>)
    if (props.point.type != 0) return (<div>
        <div className="Card--plastic--icons">
            <GenerateCups type={props.point.type} count={props.coded.amount}/>
        </div>
    </div>)
}

function Card(props) {

    console.log (props.userObject)

    let [pointObject, setPointObject] = useState(null)

    let [codedObject, setCode] = useState(null)
    let [menu, setMenu] = useState(false)

    if (pointObject == null) {
        axios.get("https://api4.simployal.com/c/" + props.id + "/info").then((response) => {
            setPointObject(response.data)
            console.log("Point info: ", response.data)
        })
    }

    if ((props.userObject != null) && (codedObject == null)) {
        let data = {
            "name": props.userObject.name,
            "googleid": props.userObject.googleId,
            "email": props.userObject.email
        }
        axios.post("https://api4.simployal.com/c/" + props.id + "/login", data).then((response) => {
                setCode(response.data)
                console.log("User info: ", response.data)
        })
    }

    console.log("Resp: ", JSON.stringify(pointObject))

    return (
        <>{pointObject && <div className="Card">
            <div className="Card--header">
                <img className="Card--header--logo" src={"https://api4.simployal.com/c/" + Number(props.id) + "/logo?size=small"}/>
                <button className="Card--header--avatar" onClick={()=>setMenu(!menu)}>
                    <img className="Card--header--avatar--icon" src={props.userObject.imageUrl}/>
                </button>
            </div>

            {(!menu && codedObject) && <div className="user-pane">

                <div className="user-pane-1">
                    <div className="Card--hello">
                        {(pointObject)? pointObject.greeting : "Добро пожаловать, "}
                        {(props.userObject)? props.userObject.name : "посетитель"}
                    </div>
                </div>

                <div className="user-pane-2">

                    <div className="Card--description">{(pointObject)? pointObject.app_text : "Назовите этот код"}</div>
                    <div className="Card--code">{JSON.stringify(codedObject.code)}</div>
                    <img className="Card--qrcode" src={"https://api.qrserver.com/v1/create-qr-code/?data=" + JSON.stringify(codedObject.code) + "&amp;size=150x150"} alt="" title="HELLO" width="100" height="100" />
                    <div className="Card--plastic">
                        <div className="Card--plastic--hint">{(pointObject)? pointObject.plastic_text : "Баланс на карте"}</div>
                        <GenerateCard point={pointObject} coded={codedObject}/>
                        <div className="Card--plastic--name">{(props.userObject)? props.userObject.name : ""}</div>
                    </div>

                    {pointObject &&<div className="Card--island--wrapper">
                        {(pointObject.tips_link != "") &&
                        <button 
                            className="Card--island blue" 
                            onClick={()=>document.location.href="https://" + pointObject.tips_link}
                        >
                            <img src={tsIcon} className="Card--island--icon"/>
                            <div className="Card--island--text">
                                {<>{pointObject.tips_name}</>}
                            </div>
                        </button>}

                        {(pointObject.instagram_link != "") &&
                        <button 
                            className="Card--island pink" 
                            onClick={()=>document.location.href="https://" + pointObject.instagram_link}
                        >
                            <img src={igIcon} className="Card--island--icon"/>
                            <div className="Card--island--text">
                                {<>{pointObject.instagram_name}</>}
                            </div>
                        </button>}

                        {(pointObject.reviews_link != "") &&
                        <button 
                            className="Card--island green" 
                            onClick={()=>document.location.href="https://" + pointObject.reviews_link}
                        >
                            <img src={rvIcon} className="Card--island--icon"/>
                            <div className="Card--island--text">
                                {<>{pointObject.reviews_name}</>}
                            </div>
                        </button>}

                        {(pointObject.website_link != "") &&
                        <button 
                            className="Card--island gray" 
                            onClick={()=>document.location.href="https://" + pointObject.website_link}
                        >
                            <img src={wsIcon} className="Card--island--icon"/>
                            <div className="Card--island--text">
                                {<>{pointObject.website_name}</>}
                            </div>
                        </button>}

                        {<img src={"https://api4.simployal.com/c/" + Number(props.id) + "/logo?size=banner"} className="Card--banner"/>}

                    </div>}
                </div>
            </div>}


            {menu && <div className="Card--menu">
                <div className="Card--hello">
                    {props.userObject.name}
                </div>
                <GoogleLogout
                    clientId={clientId}
                    buttonText="Logout"
                    onLogoutSuccess={() => {
                        props.handleChange(false, null)
                    }}
                    render={renderProps => (
                        <button className="Card--menu--logout" onClick={renderProps.onClick}>
                            {(pointObject.language == "ru") && <>Выйти</>}
                            {(pointObject.language == "en") && <>Log out</>}
                            {(pointObject.language == "pl") && <>Wylogyj</>}
                        </button>
                    )}
                />
            </div>}
        </div>}</>
    );
}

export default Card;