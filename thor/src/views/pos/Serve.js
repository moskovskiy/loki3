import { useState } from 'react';
import axios from 'axios';
import './Serve.css';

const clientId =
  '580914655403-dupoai4crpikhc1ip0kvidduqu2oir7e.apps.googleusercontent.com';

function Serve(props) {

    console.log("id = ", props.id)

    let [code, setCode] = useState(null)

    function updateCode(code) {
        setCode(code)
    }

    /* -------- */

    let [pointObject, setObject] = useState(null)

    if ((pointObject == null) && (props.id != null)) {
            axios.get("https://api4.simployal.com/c/" + props.id + "/info").then((response) => {
                setObject(response.data)
                console.log("Parsed info: ", response.data)
        })
    }

    /* -------- */

    console.log("obj = ", props.pointObject)

    let [serve, setServe] = useState(false)


    function login() {

        console.log("Logging in for id = ", props.id)

        axios({
            method: 'post',
            url: 'https://api4.simployal.com/p/2/login',
            data: {
                password:"123"
            },
            headers: {
                Authorize: ""
            }
        })
        .then((response) => {
                /*setObject(response.data)
                console.log("Parsed info: ", response.data)
                localStorage.setItem('token', response.data.token)
                props.setLoggedIn(true)*/
        })
    }

    let header = {
        "ru":"Введите код клиента",
        "en":"Enter client code",
        "pl":"Wpisz kod klienta"
    }

    return (
        <>
            {!serve && <div className="POS--container">
                <div className="POS--container--header">
                    {(pointObject)?header[pointObject.language]:"Client code"}
                </div>
                
                <input pattern="[0-9]*" text={code} autoFocus={true} onChange={(event)=>updateCode(event.target.value)} type="number" className="POS--code"/>  
            </div>}

            {serve && <div className="POS--container">
                
            </div>}

            <button className="POS--logout" onClick={(event) => {
                props.setLoggedIn(false)
                localStorage.setItem('token', null)
            }}>
                Log out
            </button>
        </>

    )
}

export default Serve;