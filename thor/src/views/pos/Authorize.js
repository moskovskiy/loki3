import { useState } from 'react';
import axios from 'axios';
import { GoogleLogin } from 'react-google-login';
import './Authorize.css';
import googleLoginIcon from '../../media/google.png';

const clientId =
  '580914655403-dupoai4crpikhc1ip0kvidduqu2oir7e.apps.googleusercontent.com';

function Authorize(props) {


    localStorage.setItem('token', null)
    console.log("Token now: ", localStorage.getItem('token'))

    let [passwd, setPasswd] = useState("")

    if (!localStorage.getItem('token')) {
        props.setLoggedIn (true)
    } else {
        props.setLoggedIn (false)
    }

    let [pointObject, setObject] = useState(null)

    if ((pointObject == null) && (props.id != null)) {
        axios.get("https://api4.simployal.com/c/" + props.id + "/info").then((response) => {
            setObject(response.data)
            console.log("Parsed info: ", response.data)
        })
    }
    
    function login() {

        console.log("Logging in for id = ", props.id)

        axios({
            method: 'post',
            url: 'https://api4.simployal.com/p/2/login',
            data: {
                password:passwd
            },
            headers: {
                Authorize: ""
            }
        })
        .then((response) => {
                console.log("Parsed info: ", response.data)
                localStorage.setItem('token', response.data.token)
                props.setLoggedIn(true)
                alert(response.data.token)
        })

    }

    return (
        <>
        <img className="Client--logo" src={"https://api4.simployal.com/c/" + Number(props.id) + "/logo?size=big"}/>
        <div className="Client--login_header">Password</div>
        <input text={passwd} onChange={(event)=>setPasswd(event.target.value)} type="password" className="POS--password"/>
        <br/>
        <button onClick={()=>login()} className="POS--login">
            Login
        </button>
        </>

    )
}

export default Authorize;