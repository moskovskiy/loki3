import React, { useState } from 'react'
import LoginView from './Authorize'
import POSView from './Serve'
import axios from 'axios'
//import ClientView from './views/client';
//import GoogleIDView from './components/GoogleIDView'

function Pos(props) {
  
  let [loggedIn, setLoggedIn] = useState(false)
  let [token, setToken] = useState(null)

  if (token == null) {
    axios.get("https://api4.simployal.com/c/" + props.match.params.point).then((response) => {
        setToken(response.data.id)
        console.log("Parsed id: ", response.data.id)
    })
  }

  /*
  if ((pointObject == null) && (props.match.params.point != null)) {
      axios.get("https://api4.simployal.com/c/" + props.match.params.point).then((response) => {
          setObject(response.data)
          console.log("Parsed info: ", response.data)
      })
  }*/

  console.log('token:', localStorage.getItem('token'))
  return (
      <div className="POS">
          {!loggedIn && <>
              <LoginView id={token} setLoggedIn={setLoggedIn}/>
          </>}
          {loggedIn && <>
              <POSView id={token} setLoggedIn={setLoggedIn}/>
          </>
          }

      </div>
  );
}

export default Pos;