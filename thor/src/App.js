import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import ClientView from './views/client';
import PosView from './views/pos';

function App() {
  return (<>
    <div className="App">
      <BrowserRouter>
        <Route exact path="/" component={() => { 
            window.location.href = 'https://about.simployal.com'; 
            return null;
        }}/>
        <Route exact path="/en" component={() => { 
            window.location.href = 'https://about.simployal.com/en'; 
            return null;
        }}/>
        <Route exact path="/ru" component={() => { 
            window.location.href = 'https://about.simployal.com/ru'; 
            return null;
        }}/>
        <Route exact path="/pl" component={() => { 
            window.location.href = 'https://about.simployal.com/pl'; 
            return null;
        }}/>

        <Route exact path="/c/:point" component={ClientView}/>
        <Route exact path="/p/:point" component={PosView}/>
      </BrowserRouter>
    </div>
  </>);
}

export default App;
