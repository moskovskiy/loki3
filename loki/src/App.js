import './App.css';
import { GoogleLogin } from 'react-google-login';
import { BrowserRouter, Route } from 'react-router-dom';
import ClientView from './views/client';
import PosView from './views/pos';
const clientId =
'580914655403-dupoai4crpikhc1ip0kvidduqu2oir7e.apps.googleusercontent.com';


function App() {
  return (<>
    <GoogleLogin
        clientId={clientId}
        buttonText="Continue with Google"
        onSuccess={(res) => {
            console.log("Logged in: " + res.profileObj)
        }}
        onFailure={(err) => {
            console.log(err)
        }}
        cookiePolicy={'single_host_origin'}
        style={{ marginTop: '100px' }}
        isSignedIn={true}

        render={renderProps => (
            <button className="login-btn" onClick={renderProps.onClick}>Продолжить с Google</button>
        )}
    />
    {/*
    <div className="App">
      <BrowserRouter>
        <Route exact path="/" component={() => { 
            window.location.href = 'https://about.simployal.com'; 
            return null;
        }}/>
        <Route exact path="/en" component={() => { 
            window.location.href = 'https://about.simployal.com/en'; 
            return null;
        }}/>
        <Route exact path="/ru" component={() => { 
            window.location.href = 'https://about.simployal.com/ru'; 
            return null;
        }}/>
        <Route exact path="/pl" component={() => { 
            window.location.href = 'https://about.simployal.com/pl'; 
            return null;
        }}/>

        <Route exact path="/c/:point" component={ClientView}/>
        <Route path="/p/:point" component={PosView}/>
      </BrowserRouter>
    </div>
      */}
  </>);
}

export default App;
