import { useState } from 'react';
import axios from 'axios';
import { GoogleLogin, GoogleLogout } from 'react-google-login';
import './Authorize.css';

const clientId =
  '580914655403-dupoai4crpikhc1ip0kvidduqu2oir7e.apps.googleusercontent.com';

/*
const GoogleAuth = (handleChange) => (
    <GoogleLogin
    clientId={clientId}
    buttonText="Continue with Google"
    onSuccess={(res) => {
        handleChange(true, res.profileObj)
        console.log("Logged in: " + res.profileObj)
    }}
    onFailure={(err) => {
        console.log(err)
    }}
    cookiePolicy={'single_host_origin'}
    style={{ marginTop: '100px' }}
    isSignedIn={true}

    render={renderProps => (
        <button className="login-btn" onClick={renderProps.onClick}>Продолжить с Google</button>
    )}
    />
)*/

function Authorize(props) {

    let [pointObject, setObject] = useState(null)

    if (pointObject == null) {
            axios.get("https://api4.simployal.com/c/" + props.id + "/info").then((response) => {
                setObject(response.data)
                console.log("Parsed info: ", response.data)
        })
    }

    console.log("Resp: ", JSON.stringify(pointObject))

    return (
        <>
        <img src={"https://api4.simployal.com/c/" + Number(props.id) + "/logo?size=big"}/>
        <div className="Client--login_header">{(pointObject)? pointObject.login_header : "Добро пожаловать!"}</div>
        <div className="Client--login_description">{(pointObject)? pointObject.login_description  : "Получайте бонусы"}</div>
        
        {(!pointObject) && <></>}
        <GoogleLogout
            clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
            buttonText="Logout"
            onLogoutSuccess={()=>{}}
        >
            
        </GoogleLogout>

        <a className="Client--service_authorize" href={"/p/"+props.id}>Вход для сотрудников</a>
        </>
    )
}

export default Authorize;