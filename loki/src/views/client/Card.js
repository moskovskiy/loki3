import { useState } from 'react';
import axios from 'axios';
import './Card.css';

function GenerateCard (type, amount, name, text) {

}

function GenerateLink (icon, name, link) {

}

function Card(props) {

    let [pointObject, setObject] = useState(null)
    let [userObject, setUser] = useState(null)

    if (pointObject == null) {
        axios.get("https://api4.simployal.com/c/" + props.id + "/info").then((response) => {
            setObject(response.data)
            console.log("Parsed info: ", response.data)
        })
    }

    if (userObject == null) {
        
        axios.get("https://api4.simployal.com/c/" + props.id + "/login").then((response) => {
                setObject(response.data)
                console.log("Parsed info: ", response.data)
        })
    }

    console.log("Resp: ", JSON.stringify(pointObject))

    return (
        <div className="Card">
            <div className="Card--header">
                <img className="Card--header--logo" src={"https://api4.simployal.com/c/" + Number(props.id) + "/logo?size=small"}/>
                <img className="Card--header-avatar" src="png"/>
            </div>
            <div className="Card--content">
                <div className="Card--description">{(pointObject)? pointObject.app_text : "Назовите этот код"}</div>
            </div>
        </div>
    );
}

export default Card;